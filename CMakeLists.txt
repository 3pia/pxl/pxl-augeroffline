cmake_minimum_required (VERSION 2.6)
project (PXL-ROOT)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}")
set(OFFLINE_LIBRARY_DIR /home/cglaser/software/Offline/lib)
set(OFFLINE_INCLUDE_DIR /home/cglaser/software/Offline/include/adst)

find_package(ROOT)
find_package(PXL)
INCLUDE (Python.cmake)

# PXL plugin name
SET(PXLMODULENAME pxl-augeroffline)

#PXL plugin needed here
ADD_PXL_PLUGIN(pxl-core)

include_directories (
    include 
		${PXL_INCLUDE_DIRS}
    ${CMAKE_BINARY_DIR}/include
    ${ROOT_INCLUDE_DIR}
    ${OFFLINE_INCLUDE_DIR}
		${PYTHON_INCLUDE_PATH}
) 

link_directories(${PXL_LIBRARIES_DIR} ${ROOT_LIBRARY_DIR} ${OFFLINE_LIBRARY_DIR})

# Library that includes the new data type
add_library (pxl-augeroffline SHARED
	src/AugerOfflineSerializable.cc
)
target_link_libraries(pxl-augeroffline pxl-core ${ROOT_LIBRARIES})

#pxl plugin
add_library (AugerOfflinePlugin SHARED
	src/AugerOffline.cc
)
target_link_libraries(AugerOfflinePlugin pxl-augeroffline ${ROOT_LIBRARIES})

SET_TARGET_PROPERTIES(${PXLMODULENAME} PROPERTIES COMPILE_FLAGS -fPIC)
SET_TARGET_PROPERTIES(AugerOfflinePlugin PROPERTIES COMPILE_FLAGS -fPIC)

INSTALL(TARGETS AugerOfflinePlugin LIBRARY DESTINATION ${PXL_PLUGIN_INSTALL_PATH})
INSTALL(TARGETS pxl-augeroffline LIBRARY DESTINATION
	${CMAKE_INSTALL_PREFIX}/lib)


#
# SWIG
#
FIND_PACKAGE(SWIG REQUIRED)
INCLUDE(${SWIG_USE_FILE})

IF(SWIG_version_output VERSION_LESS 1.3.30)
    SET(SWIG_SELF "self")
ELSE()
    SET(SWIG_SELF "$self")
ENDIF()

SET(SWIG_FLAGS "-Wall" "-fvirtual" "-I${PXL_DATA_DIR}")

CONFIGURE_FILE(pxl-augeroffline.i.in ${CMAKE_SOURCE_DIR}/pxlaugeroffline.i)

SET(SWIG_MODULE_${PXLMODULENAME}_EXTRA_DEPS "include/pxl/pxl-augeroffline.hh" )
SET_SOURCE_FILES_PROPERTIES("pxlaugeroffline.i" PROPERTIES CPLUSPLUS ON)

SET_PROPERTY(SOURCE pxlroot.i APPEND PROPERTY SWIG_FLAGS ${SWIG_FLAGS})
SWIG_ADD_MODULE(pxlaugeroffline python pxlaugeroffline.i
	${PXL_INCLUDE_DIRS} ".")
target_link_libraries(_pxlaugeroffline ${PXLMODULENAME} ${PYTHON_LIBRARIES} AugerOfflinePlugin pxl-augeroffline pxl-core ${ROOT_LIBRARIES} PyROOT)

INSTALL(FILES "pxlaugeroffline.py" DESTINATION ${PYTHON_SITE_PACKAGES})
INSTALL(FILES "_pxlaugeroffline.so" DESTINATION ${PYTHON_SITE_PACKAGES})

enable_testing()
CONFIGURE_FILE(pypxl_testAugerOffline.py.in ${CMAKE_SOURCE_DIR}/pypxl_testAugerOffline.py)
ADD_TEST(PythonRoot ${PYTHON_EXECUTABLE}
	${CMAKE_CURRENT_BINARY_DIR}/pypxl_testAugerOffline.py)

