//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef INITIALIZE_AUGEROFFLINE_HH_
#define INITIALIZE_AUGEROFFLINE_HH_

#include "pxl/core/macros.hh"

namespace pxl
{
class PXL_DLL_EXPORT AugerOffline
{
public:
	static void initialize();
	static void shutdown();

};
}

#endif /* INITIALIZE_AUGEROFFLINE_HH_ */
