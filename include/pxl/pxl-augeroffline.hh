//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef PXL_AUGEROFFLINE_HH
#define PXL_AUGEROFFLINE_HH

#include "pxl/AugerOfflineSerializable.hh"
#include "pxl/AugerOffline.hh"

#endif // PXL_AUGEROFFLINE_HH
