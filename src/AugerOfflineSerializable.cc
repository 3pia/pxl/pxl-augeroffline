//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include <iostream>
#include <stdexcept>

#include "pxl/AugerOfflineSerializable.hh"
#include "pxl/core/logging.hh"

#include "TBufferFile.h"
#include "TClass.h"

static pxl::Logger logger("pxl::AugerOfflineSerializable");

namespace pxl {
AugerOfflineSerializable::AugerOfflineSerializable(const AugerOfflineSerializable& original) :
		Serializable(), _name(original._name), _recEvent(0), _detectorGeometry(
				0) {
	if (original._tobject)
		_tobject = original._tobject->Clone();
}
AugerOfflineSerializable::AugerOfflineSerializable(const AugerOfflineSerializable* original) :
		Serializable(), _name(original->_name), _recEvent(0), _detectorGeometry(
				0) {
	if (original->_tobject)
		_tobject = original->_tobject->Clone();
}
AugerOfflineSerializable::AugerOfflineSerializable(TObject* tobj, const std::string& name) :
		Serializable(), _name(name), _recEvent(0), _detectorGeometry(0) {
	if (tobj)
		_tobject = tobj;
}
AugerOfflineSerializable::~AugerOfflineSerializable() {
	if (_tobject) {
		delete _tobject;
		_tobject = 0;
	}
	if (_recEvent) {
		delete _recEvent;
		_recEvent = 0;
	}
	if (_detectorGeometry) {
		delete _detectorGeometry;
		_detectorGeometry = 0;
	}
}

void AugerOfflineSerializable::setTObject(TObject* tobj) {
	if (_tobject) {
		delete _tobject;
	}
	_tobject = tobj;
}

void AugerOfflineSerializable::setRecEvent(RecEvent* event) {
	if(_recEvent) {
		delete _recEvent;
	}
	_recEvent = event;
}

void AugerOfflineSerializable::setDetectorGeometry(DetectorGeometry* detectorGeometry) {
	if(_detectorGeometry) {
		delete _detectorGeometry;
	}
	_detectorGeometry= detectorGeometry;
}

void AugerOfflineSerializable::serialize(const OutputStream &out) const {
	Serializable::serialize(out);
	out.writeString(_name);

	if (_tobject) {
		std::string className(_tobject->ClassName());
		out.writeString(className);

		if (className == "")
			return;

		// Buffer size may be optimised, but 2k seems reasonable for a simple object
		const Int_t bufsize = 2048;

		TBufferFile buffer(TBuffer::kWrite, bufsize);
		buffer.MapObject(_tobject);
		_tobject->Streamer(buffer);

		std::string objStr(buffer.Buffer(), buffer.Length());
		out.writeString(objStr);
	} else {
		out.writeString("");
	}

}

void AugerOfflineSerializable::deserialize(const InputStream &in) {
	Serializable::deserialize(in);
	in.readString(_name);

	std::string className;
	in.readString(className);

	if (className == "")
		return;

	std::string objStr;
	in.readString(objStr);

	char* buffer = new char[objStr.size()]; // no delete needed as TBufferFile takes ownership
	objStr.copy(buffer, objStr.size());

	const Int_t bufsize = objStr.size();

	TBufferFile bufferFile(TBuffer::kRead, bufsize);
	bufferFile.SetBuffer(buffer); // ownership taken here

	TClass *cl = TClass::GetClass(className.c_str());

	char* pobj = (char*) cl->New();
	Int_t baseOffset = cl->GetBaseClassOffset(TObject::Class());

	TObject* obj = (TObject*) (pobj + baseOffset);
	obj->Streamer(bufferFile);
	_tobject = obj;
}
TObject* AugerOfflineSerializable::takeTObject() {
	if (!_tobject)
		return 0;

	TObject* tmpObj = _tobject;
	_tobject = 0;
	return tmpObj;
}

std::ostream& AugerOfflineSerializable::print(int level, std::ostream& os,
		int pan) const {
	return os << "pxl::AugerOfflineSerializable with id: " << id() << std::endl;
}

} // namespace pxl

std::ostream& operator<<(std::ostream& cxxx,
		const pxl::AugerOfflineSerializable& obj) {
	return obj.print(0, cxxx, 0);
}
