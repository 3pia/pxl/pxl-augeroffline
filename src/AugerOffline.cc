//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "pxl/core.hh"
#include "pxl/AugerOffline.hh"
#include "pxl/AugerOfflineSerializable.hh"

namespace pxl
{

static bool _initialized = false;

static ObjectProducerTemplate<AugerOfflineSerializable> _AugerOfflineSerializableProducer;

void AugerOffline::initialize()
{
	if (_initialized)
		return;

	_AugerOfflineSerializableProducer.initialize();
	_initialized = true;
}

void AugerOffline::shutdown()
{
	if (_initialized == false)
		return;
	_AugerOfflineSerializableProducer.shutdown();
	_initialized = false;
}

} // namespace pxl
